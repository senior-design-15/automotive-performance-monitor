/*
 *GPS TASK FILE
 *Brayden m
 *3/25/2024
 * this task connects to the gps module via a serial connection
 * then continues to push gps data packets down a queue.
 */

#include <Arduino.h>
#include <HardwareSerial.h>
#include <TinyGPSPlus.h>
#include "gps_task.hpp"
#include "ui_task.hpp"
#include "apm_pindef.hpp"

TinyGPSPlus gps;
bool GPS_COMMAND_READY = false;

void gpsMainTask(void *pvParameters)
{
    bool gpsConnection = false;
    Serial2.begin(115200, SERIAL_8N1, GPS_TX_PIN, GPS_RX_PIN);
    delay(1000);
    
    GPS_COMMAND_READY = true;

    while (true)
    {
        while (Serial2.available())
        {
            gps.encode(Serial2.read());
        }

        if (uxQueueSpacesAvailable(GPS_DATA_QUEUE) != 0 && gps.location.isUpdated())
        {

            gpsDataPack newDataPack = gpsPacketBuild();

            if (xQueueSend(GPS_DATA_QUEUE, &newDataPack, (TickType_t)100) != pdPASS)
            {
                printf("send to queue failed\n");
            }
        }
        else
        {
            vTaskDelay(pdMS_TO_TICKS(10));
        }
    }
}

gpsDataPack gpsPacketBuild()
{
    gpsDataPack DataPack;

    DataPack.latDegree = (float)(gps.location.lat());
    DataPack.lonDegree = (float)(gps.location.lng());
    DataPack.hours = gps.time.hour();
    DataPack.minutes = gps.time.minute();
    DataPack.seconds = gps.time.second();
    //DataPack.groundSpeed = ((double)gps.speed.value() / 100) * 1.15078;
    DataPack.groundSpeed = gps.speed.value();
    DataPack.numSats = gps.satellites.value();
    DataPack.hdop = gps.hdop.value();

    return DataPack;
}
