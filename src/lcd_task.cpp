#include <Arduino.h>
#include "lcd_task.hpp"
#include "apm_pindef.hpp"
#include <Wire.h>

#include "freertos/queue.h"

QueueHandle_t LCD_DATA_QUEUE;
bool LCD_TASK_READY = false;

void lcd_init() {
    
    if(!Wire.begin(LCD_SDA, LCD_SCL, 400000)) {
        Serial.printf("LCD_TASK: Failed to initialize I2C interface! halting\n");
        while(1) {vTaskDelay(pdMS_TO_TICKS(1000));}
    }

    LCD_DATA_QUEUE = xQueueCreate(3, sizeof(lcd_data_t));
    if(LCD_DATA_QUEUE == 0) {
        Serial.printf("LCD_TASK: Failed to initialize data queue! halting\n");
        while(1) {vTaskDelay(pdMS_TO_TICKS(1000));}
    }

    Serial.printf("LCD_TASK: Task ready!\n");
    LCD_TASK_READY = true;

}

void read_queues(lcd_data_t* a, lcd_data_t* b, lcd_data_t* c) {
    
    if( xQueueReceive(LCD_DATA_QUEUE, a, pdMS_TO_TICKS(10)) != pdPASS ) {
        Serial.printf("LCD_TASK: Failed to read first data packet!");
        memset(a, 0, sizeof(lcd_data_t));
    }

    if( xQueueReceive(LCD_DATA_QUEUE, b, pdMS_TO_TICKS(10)) != pdPASS ) {
        Serial.printf("LCD_TASK: Failed to read second data packet!");
        memset(b, 0, sizeof(lcd_data_t));
    }

    if( xQueueReceive(LCD_DATA_QUEUE, c, pdMS_TO_TICKS(10)) != pdPASS ) {
        Serial.printf("LCD_TASK: Failed to read third data packet!");
        memset(c, 0, sizeof(lcd_data_t));
    }

}

void lcd_main() {
    
    lcd_init();

    lcd_data_t packet_a;
    lcd_data_t packet_b;
    lcd_data_t packet_c;

    memset(&packet_a, 0, sizeof(lcd_data_t));
    memset(&packet_b, 0, sizeof(lcd_data_t));
    memset(&packet_c, 0, sizeof(lcd_data_t));

    uint8_t raw_data[sizeof(lcd_data_t)] = {0};
    uint8_t tx_status = 0;

    while(1) {

        // If we have all three data points
        if (uxQueueSpacesAvailable(LCD_DATA_QUEUE) == 0) {

            // Read from the queues into the packets
            read_queues(&packet_a, &packet_b, &packet_c);

            // Serialize first packet and transmit
            memcpy(raw_data, &packet_a, sizeof(lcd_data_t));
            Wire.beginTransmission(LCD_A_ADDR);
            for(int i = 0; i < sizeof(lcd_data_t); i++) {
                Wire.write(raw_data[i]);
            }
            Wire.endTransmission(true);

            // Reset buffer, copy new data, and transmit to second LCD
            memset(raw_data, 0, sizeof(lcd_data_t));
            memcpy(raw_data, &packet_b, sizeof(lcd_data_t));
            Wire.beginTransmission(LCD_B_ADDR);
            for(int i = 0; i < sizeof(lcd_data_t); i++) {
                Wire.write(raw_data[i]);
            }
            Wire.endTransmission(true);

            // Reset buffer, copy new data, and transmit to third LCD
            memset(raw_data, 0, sizeof(lcd_data_t));
            memcpy(raw_data, &packet_c, sizeof(lcd_data_t));
            Wire.beginTransmission(LCD_C_ADDR);
            for(int i = 0; i < sizeof(lcd_data_t); i++) {
                Wire.write(raw_data[i]);
            }
            Wire.endTransmission(true);
            
            // Reset data points
            memset(&packet_a, 0, sizeof(lcd_data_t));
            memset(&packet_b, 0, sizeof(lcd_data_t));
            memset(&packet_c, 0, sizeof(lcd_data_t));
            
        }

        vTaskDelay(pdMS_TO_TICKS(250));
    }
}