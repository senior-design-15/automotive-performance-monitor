#include <Arduino.h>
#include <string.h>
#include <esp_err.h>
#include "driver/twai.h"
#include "obd_lib.hpp"

// Used to indicate ISO-TP frame type
typedef enum {
    SINGLE,
    FIRST,
    CONSEC,
    CONTROL,
    UNDEF
} iso_frame_type_t;

// Reserve a 2D array for the raw message data
// Accessing like RX_DATA[i] gives the i'th message's data
#define RX_MSG_MAX 7
uint8_t RX_MSG_CNT;
uint8_t RX_DATA[RX_MSG_MAX][8];

// Buffer for the data derrived from the ISO-TP data
// This is oversized a bit but oh well. Math hard.
uint16_t DECODED_DATA_LEN;
uint16_t DECODED_DATA_IDX;
uint8_t DECODED_DATA[8 * RX_MSG_MAX];

// Count of decoded DTCs
uint16_t DTC_CNT;

/**
 * Determines an ISO-TP frame type given an 
 * ISO-TP message 
 */
iso_frame_type_t frame_type(uint8_t first_byte) {

    switch(first_byte >> 4) {
        case 0:
            return SINGLE;
            break;
        
        case 1:
            return FIRST;
            break;
        
        case 2:
            return CONSEC;
            break;
        
        case 3:
            return CONTROL;
            break;
        
        default:
            return UNDEF;
    }

    return SINGLE;
}

/**
 * Processes a received single frame "packet." Appends the relevant message
 * data to the decoded data buffer.
 */
esp_err_t tp_process_sf(uint8_t data[8]) {

    // Negative response check
    if( (data[1] >> 4) != 0x4 ) {
        return ESP_ERR_INVALID_RESPONSE;
    }

    DECODED_DATA_LEN = (data[0] & 0x0F);
    memcpy(DECODED_DATA, &data[1], DECODED_DATA_LEN);
    DECODED_DATA_IDX = DECODED_DATA_LEN;

    return ESP_OK;
}

/**
 * Process a received ISO-TP first frame.
 * Decodes message length and initializes the
 * decoded data buffers, and copies the relevant data
 * into said buffers.
 */
esp_err_t tp_process_ff(uint8_t data[8]) {

    // Reset decoded data buffer
    DECODED_DATA_IDX = 0;
    memset(DECODED_DATA, 0, 8*RX_MSG_MAX); 

    // Extract length of decoded data
    DECODED_DATA_LEN = ((data[0] & 0x0F) << 12) | data[1];

    // Warning if decoded data length exceeds our buffer
    if(DECODED_DATA_LEN > 8*RX_MSG_MAX) {
        Serial.printf("TP_LIB: Buffer overflow imminent, supposed data length exceeds preallocated buffer\n");
        return ESP_ERR_INVALID_SIZE;
    }

    // Insert this frame's data at the beginning of the decoded data buffer
    // and increment decoded data index accordingly
    memcpy(&DECODED_DATA[DECODED_DATA_IDX], &data[2], 6);
    DECODED_DATA_IDX += 6;

    return ESP_OK;

}

/**
 * Process a received ISO-TP consecutive frame.
 * Assuming there wasn't a sequence error, this
 * method appends the relevant data to the decoded data buffers.
 */
esp_err_t tp_process_cf(uint8_t data[8], uint8_t expected_bsc) {
    
    // Verify sequence
    uint8_t actual_bsc = data[0] & 0xF;
    if(actual_bsc != expected_bsc) {
        Serial.printf("TP_LIB: Sequence error! Expected %d but got %d\n", expected_bsc, actual_bsc);
        return ESP_ERR_INVALID_STATE;
    }

    uint16_t remaining_len = DECODED_DATA_LEN - DECODED_DATA_IDX;

    // Copy in the whole message if all of the data will fit
    if(remaining_len >= 7) {
        memcpy(&DECODED_DATA[DECODED_DATA_IDX], &data[1], 7);
        DECODED_DATA_IDX += 7;

    // Otherwise just copy what will fit
    } else {
        memcpy(&DECODED_DATA[DECODED_DATA_IDX], &data[1], remaining_len);
        DECODED_DATA_IDX += remaining_len;
    }

    return ESP_OK;
}

/**
 * Decodes a provided DTC which is in its raw hex form
 * into a 5-character string (plus a null terminator).
 * 
 * @param dtc the numerical representation of the DTC
 * @param string the string where the output DTC will be placed. Note that
 *                  DTCs are five chars long, plus one for the null terminator
 */
void dtc_to_string(uint16_t dtc, char string[6]) {
    memset(string, 0, 6);

    char letter;
    uint8_t char_mask = dtc >> 14;
    uint8_t thousands = (dtc >> 12) & 0b11;
    uint8_t hundreds  = (dtc >> 8) & 0xF;
    uint8_t tens      = (dtc >> 4) & 0xF;
    uint8_t ones      = dtc & 0xF;

    switch(char_mask) {
        case 0:
            letter = 'P';
            break;
        case 1:
            letter = 'C';
            break;
        case 2:
            letter = 'B';
            break;
        case 3:
            letter = 'U';
            break;
        default:
            letter = 'X';
            break;
    }

    sprintf(string, "%c%X%X%X%X", letter, thousands, hundreds, tens, ones);

}

/**
 * Helper method to transmit a flow control frame. Basically used
 * when you receive a first frame to let the other end of the communication
 * link know that you're ready for the rest of the message. 
 */
esp_err_t tp_control_tx() {
    
    // Data for a flow control frame
    static const uint8_t fc_data[8] = {0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    // Build flow control frame
    twai_message_t fc_frame;
    fc_frame.extd = 0;
    fc_frame.rtr = 0;
    fc_frame.ss = 0;
    fc_frame.self = 0;
    fc_frame.dlc_non_comp = 0;
    fc_frame.identifier = 0x7DF; // OBD TX ID
    fc_frame.data_length_code = 8;
    memcpy(fc_frame.data, fc_data, 8);

    return twai_transmit(&fc_frame, pdMS_TO_TICKS(10));
    
}

/**
 * Common functionality extracted from the different types of DTC requests
 */
esp_err_t dtc_rx(uint16_t* dtc_out, uint8_t out_max, uint8_t* actual_cnt) {

    // populate_test_data();

    // Loop through receiving DTCs until we timeout
    twai_message_t rx_message;
    while( twai_receive(&rx_message, pdMS_TO_TICKS(1000)) == ESP_OK ) {
        memcpy(RX_DATA[RX_MSG_CNT], rx_message.data, 8);
        RX_MSG_CNT++;

        // If the frame type is a FF send a FC
        if( frame_type(rx_message.data[0]) == FIRST ) {
            esp_err_t FC_STATUS = tp_control_tx();
            if(FC_STATUS != ESP_OK) {
                Serial.printf("TP_LIB: Error transmitting a control frame\n");
                return FC_STATUS;
            }
        }

        // Too many messages?
        if(RX_MSG_CNT == RX_MSG_MAX) {
            break;
        }

    }

    // If we received too many DTCs
    if(RX_MSG_CNT == RX_MSG_MAX) {
        Serial.printf("TP_LIB: Received too many DTCs, go to a mechanic bro\n");
        return ESP_ERR_INVALID_SIZE;
    }

    // If we received no DTCs
    if(RX_MSG_CNT == 0) {
        Serial.printf("TP_LIB: Received no DTCs, congrats!\n");
        *actual_cnt = 0;
        return ESP_OK;
    }

    // Block sequence counter for multi-frame messages
    uint8_t TP_BSC = 0;

    // Decoding status
    esp_err_t RX_DECODE_STATUS = ESP_OK;

    // Loop thru all RX'd messages
    for(int i = 0; i < RX_MSG_CNT; i++) {

        // Decode current frame given the first byte of the message
        iso_frame_type_t current_frame = frame_type(RX_DATA[i][0]);

        // Do stuff based on frame type
        switch(current_frame) {
            
            // Single frame, all data fits into one CAN message
            case SINGLE:

                RX_DECODE_STATUS = tp_process_sf(RX_DATA[i]);
                if(RX_DECODE_STATUS != ESP_OK) {
                    return RX_DECODE_STATUS;
                }

                break;
            
            // First frame, data will be split across many messages
            case FIRST:

                // Process first frame
                RX_DECODE_STATUS = tp_process_ff(RX_DATA[i]);
                if(RX_DECODE_STATUS != ESP_OK) {
                    return RX_DECODE_STATUS;
                }

                // Set the expected value for the next block
                // sequence count value
                TP_BSC = 1;

                break;
            
            // Control frame, which we shouldn't receive ever
            case CONTROL:
                Serial.printf("TP_LIB: Received a control frame??? We shouldn't but ok I guess\n");
                return ESP_ERR_INVALID_STATE;
                break;
            
            // Consecutive frame following a first frame
            case CONSEC:

                // Process the consecutive frame
                RX_DECODE_STATUS = tp_process_cf(RX_DATA[i], TP_BSC);
                if(RX_DECODE_STATUS != ESP_OK) {
                    return RX_DECODE_STATUS;
                }

                // Increment block sequence counter
                TP_BSC++;

                break;
            
            // Probably not an ISO-TP stuff
            default:
                Serial.printf("TP_LIB: Frame %d is probably not an ISO-TP frame\n", i);
                return ESP_ERR_INVALID_ARG;
                break;
        }
    }

    // Length check
    if(DECODED_DATA_IDX != DECODED_DATA_LEN) {
        Serial.printf("TP_LIB: Some packets dropped! Expected %d bytes but received only %d\n", DECODED_DATA_LEN, DECODED_DATA_IDX);
        *actual_cnt = 0;
        return ESP_ERR_INVALID_SIZE;
    }

    // Check for a positive response indicator
    if( (DECODED_DATA[0] >> 4) != 0x4 ) {
        Serial.printf("TP_LIB: Response is a negative response\n");
        *actual_cnt = 0;
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Assuming we got a positive response, we can 
    // discard the first two bytes
    DECODED_DATA_LEN -= 2;
    memcpy(DECODED_DATA, &DECODED_DATA[2], DECODED_DATA_LEN); // Shift left to discard first two bytes

    // Each DTC is represented by two bytes, so halve the buffer length
    DTC_CNT = DECODED_DATA_LEN/2;
    
    // Output buffer length check
    if(DTC_CNT > out_max) {
        Serial.printf("TP_LIB: Decoded DTCs won't fit into the provided buffer");
        *actual_cnt = 0;
        return ESP_ERR_INVALID_SIZE;
    }

    // Clear provided array
    memset(dtc_out, 0, DTC_CNT * sizeof(uint16_t));

    // Combine two bytes to make the resulting DTC
    for(int i = 0; i < DECODED_DATA_LEN; i+=2) {
        dtc_out[i/2] = ( DECODED_DATA[i] << 8 ) | DECODED_DATA[i+1];
    }

    // Update the count of actual DTCs
    *actual_cnt = DTC_CNT;

    return ESP_OK;

}

/**
 * Requests the list of active DTCs (also known as "stored" DTCs) which are
 * the codes that are causing your check engine light to be illuminated 
 */
esp_err_t get_active_dtc(uint16_t* dtc_out, uint8_t out_max, uint8_t* actual_cnt) {

    // Build OBD-II request
    obd2_t tx_request;
    tx_request.mode = STR_DTC;
    tx_request.PID = 0x00;
    tx_request.len = 0;

    // TX Request
    esp_err_t TX_STATUS = obd2_tx(&tx_request);
    if(TX_STATUS != ESP_OK) {
        Serial.printf("TP_LIB: Error transmitting stored DTC request\n");
        return TX_STATUS;
    }

    // Common functionality
    return dtc_rx(dtc_out, out_max, actual_cnt);

}

/**
 * Requests the list of permanent DTCs (stored or active DTCs which the user has cleared)
 * Note that not all cleared DTCs are permanently kept, some of them actually can
 * disappear after being erased.
 */
esp_err_t get_perm_dtc(uint16_t* dtc_out, uint8_t out_max, uint8_t* actual_cnt) {

    // Build OBD-II request
    obd2_t tx_request;
    tx_request.mode = PRM_DTC;
    tx_request.PID = 0x00;
    tx_request.len = 0;

    // TX Request
    esp_err_t TX_STATUS = obd2_tx(&tx_request);
    if(TX_STATUS != ESP_OK) {
        Serial.printf("TP_LIB: Error transmitting permanent DTC request\n");
        return TX_STATUS;
    }

    // Common functionality
    return dtc_rx(dtc_out, out_max, actual_cnt);

}

/**
 * Requests the list of pending DTCs (DTCs that are about to become active or stored)
 * My assumption is that some DTCs need to occurr multiple times in order for the ECU
 * to justify illuminating the check engine light 
 */
esp_err_t get_pend_dtc(uint16_t* dtc_out, uint8_t out_max, uint8_t* actual_cnt) {

    // Build OBD-II request
    obd2_t tx_request;
    tx_request.mode = PEND_DTC;
    tx_request.PID = 0x00;
    tx_request.len = 0;

    // TX Request
    esp_err_t TX_STATUS = obd2_tx(&tx_request);
    if(TX_STATUS != ESP_OK) {
        Serial.printf("TP_LIB: Error transmitting pending DTC request\n");
        return TX_STATUS;
    }

    // Common functionality
    return dtc_rx(dtc_out, out_max, actual_cnt);

}

/**
 * For some reason, the flow control frame for a VIN request
 * is slightly different than a standard flow control.
 * For the other messages that require a flow control, CAN ID
 * 0x7DF works fine but for VIN, I am reading that the ID needs
 * to be 0x7E0 for some reason.
 */
esp_err_t vin_fc_tx() {
    
    // Data for a flow control frame
    static const uint8_t fc_data[8] = {0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    // Build flow control frame
    twai_message_t fc_frame;
    fc_frame.extd = 0;
    fc_frame.rtr = 0;
    fc_frame.ss = 0;
    fc_frame.self = 0;
    fc_frame.dlc_non_comp = 0;
    fc_frame.identifier = 0x7E0; // Again, for some reason this ins't 7DF
    fc_frame.data_length_code = 8;
    memcpy(fc_frame.data, fc_data, 8);

    return twai_transmit(&fc_frame, pdMS_TO_TICKS(10));
}

void populate_vin_data() {
    static const uint8_t msg_00[] = {0x10, 0x14, 0x49, 0x02, 0x01, 0x34, 0x53, 0x33};
    static const uint8_t msg_01[] = {0x21, 0x42, 0x4E, 0x42, 0x4A, 0x36, 0x34, 0x46};
    static const uint8_t msg_02[] = {0x22, 0x33, 0x30, 0x37, 0x33, 0x34, 0x37, 0x30};

    memcpy(RX_DATA[0], msg_00, 8);
    memcpy(RX_DATA[1], msg_01, 8);
    memcpy(RX_DATA[2], msg_02, 8);
    RX_MSG_CNT = 3;
}

// Service 09
// PID 02
// Should return a 17-char VIN in ASCII (might be zero-padded from the MSB???)
// VIN should only be 17 ascii chars but allocate 24 to be safe
esp_err_t get_vin(char str_out[24]) {

    // Clear existing string
    memset(str_out, 0, 24);

    // Clear existing decoded data
    memset(DECODED_DATA, 0, (8 * RX_MSG_MAX));
    DECODED_DATA_LEN = 0;
    DECODED_DATA_IDX = 0;

    // Reset RX Message count and clear buffer
    RX_MSG_CNT = 0;
    for(int i = 0; i < 7; i++) {
        uint8_t* data = RX_DATA[i];
        memset(data, 0, 8);
    }

    // Build OBD-II request
    obd2_t tx_request;
    tx_request.mode = REQ_INF;
    tx_request.PID = 0x02;
    tx_request.len = 0;

    // TX Request
    esp_err_t TX_STATUS = obd2_tx(&tx_request);
    if(TX_STATUS != ESP_OK) {
        Serial.printf("TP_LIB: Error transmitting VIN request\n");
        Serial.printf("TP_LIB: Error code %s\n", esp_err_to_name(TX_STATUS));
        return TX_STATUS;
    }

    twai_message_t rx_message;
    esp_err_t decode_status = ESP_OK;

    // Receive messages and place their data into a buffer
    while( twai_receive(&rx_message, pdMS_TO_TICKS(1000)) == ESP_OK ) {

        twai_print(&rx_message, true);

        // If the frame type is a FF send a FC
        if( frame_type(rx_message.data[0]) == FIRST ) {
            esp_err_t FC_STATUS = vin_fc_tx();
            if(FC_STATUS != ESP_OK) {
                Serial.printf("TP_LIB: Error transmitting a control frame\n");
                return FC_STATUS;
            }
        }

        if(RX_MSG_CNT == RX_MSG_MAX) {
            break;
        }

    }

    if(RX_MSG_CNT == 0) {
        Serial.printf("TP_LIB: Didn't get any packets after VIN request\n");
        return ESP_FAIL;
    }

    // populate_vin_data();

    esp_err_t STATUS = ESP_OK;
    uint8_t TP_BSC = 0;

    for(int i = 0; i < RX_MSG_CNT; i++) {

        uint8_t* current_msg = RX_DATA[i];
        iso_frame_type_t type = frame_type(current_msg[0]);

        switch(type) {
            case SINGLE:
                Serial.printf("TP_LIB: Should not have RX'd a VIN in a single frame\n");
                return ESP_FAIL;
                break;

            case FIRST:
                STATUS = tp_process_ff(current_msg);
                if(STATUS != ESP_OK) {
                    return STATUS;
                }
                TP_BSC = 1;
                break;

            case CONSEC:
                STATUS = tp_process_cf(current_msg, TP_BSC);
                if(STATUS != ESP_OK) {
                    return STATUS;
                }
                TP_BSC++;
                break;

            case CONTROL:
                Serial.printf("TP_LIB: Should not have gotten a control flow frame\n");
                return ESP_FAIL;
                break;
            default:
                Serial.printf("TP_LIB: Got an invalid TP frame type of 0x%02X\n", current_msg[0]);
                return ESP_FAIL;
        }

    }

    // Length check
    if(DECODED_DATA_IDX != DECODED_DATA_LEN) {
        Serial.printf("TP_LIB: Some packets dropped! Expected %d bytes but received only %d\n", DECODED_DATA_LEN, DECODED_DATA_IDX);
        return ESP_ERR_INVALID_SIZE;
    }

    // Check for a positive response indicator
    if( (DECODED_DATA[0] >> 4) != 0x4 ) {
        Serial.printf("TP_LIB: Response is a negative response\n");
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Assuming we got a positive response, we can 
    // discard the first two bytes
    DECODED_DATA_LEN -= 2;
    memcpy(DECODED_DATA, &DECODED_DATA[2], DECODED_DATA_LEN); // Shift left to discard first two bytes

    
    uint8_t TEMP_STR[24] = {0};
    memset(TEMP_STR, 0, 24);

    // There might be a SOH or hex 0x01 at the beginning of the string?
    if(DECODED_DATA[0] == 0x01) {
        memcpy(str_out, &DECODED_DATA[1], 17);
    } else if(isAlphaNumeric(DECODED_DATA[0])) {
        memcpy(str_out, DECODED_DATA, 17);
    }

    Serial.printf("TP_LIB: Decoded VIN is %s\n", str_out);

    return ESP_OK;

}

esp_err_t clear_dtcs() {

    // Build OBD-II request
    obd2_t tx_request;
    tx_request.mode = CLR_DTC;
    tx_request.PID = 0x00;
    tx_request.len = 0;

    // TX Request
    esp_err_t TX_STATUS = obd2_tx(&tx_request);
    if(TX_STATUS != ESP_OK) {
        Serial.printf("TP_LIB: Error transmitting clear DTC request\n");
        Serial.printf("%s\n", esp_err_to_name(TX_STATUS));
        clear_dtcs();
        return TX_STATUS;
    }

    return ESP_OK;
}