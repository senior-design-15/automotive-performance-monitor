#include <Arduino.h>

// FreeRTOS stuff
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

// Peripheral stuff
#include "esp_err.h"
#include "driver/gpio.h"
#include "driver/twai.h"

// The OBD-II Library stuff
#include "obd_types.hpp"
#include "obd_lib.hpp"
#include "iso_tp.hpp"
#include "apm_pindef.hpp"

#include "ui_task.hpp"

// Indicator that other tasks can use to see if this task is ready
bool OBD_LIB_READY = false;

// The PIDs being actively monitored
uint8_t PID_A, PID_B, PID_C;

// Commands coming into this task from other tasks
QueueHandle_t OBD_COMMAND_QUEUE;

// Flag to determine if we should monitor PIDs
bool isMonitoring = false;

const char* pid_to_str(uint8_t pid) {
    
    switch(pid) {
        
        case PID_ENGINE_LOAD_PCT:
            return "Engine load [%]";
            break;

        case PID_ENGINE_COOLANT_TEMP:
            return "Engine coolant temperature, celsius";
            break;

        case PID_FUEL_PRESSURE:
            return "Fuel pressure, kPa";
            break;

        case PID_INTAKE_MANUFOLD_PRESSURE:
            return "Intake manufold pressure, kPa";
            break;

        case PID_ENGINE_RPM:
            return "Engine speed, RPM";
            break;

        case PID_VEHICLE_SPEED:
            return "Wheel speed, km/h";
            break;

        case PID_INTAKE_AIR_TEMP:
            return "Intake air temperature, celsius";
            break;

        case PID_AIR_FLOW_RATE:
            return "Intake air flow rate, g/s";
            break;

        case PID_THROTTLE_POS:
            return "Throttle position [%]";
            break;

        case PID_ENGINE_OIL_TEMP:
            return "Engine oil temperature, celsius";
            break;

        default:
            return "Unknown reading";

    }
}

void obd_task_init() {
    
    // Constants for configurations
    static const uint32_t ALERT_MASK = 0x1FFF0;
    static const uint32_t obd2_acceptance_code = 0x7E8 << 21;
    static const uint32_t obd2_acceptance_mask = 0x1FFFFF;

    // Configuration structs
    static twai_general_config_t general_config;
    static twai_timing_config_t timing_config;
    static twai_filter_config_t filter_config;

    // Use macros to define these two
    general_config = TWAI_GENERAL_CONFIG_DEFAULT(CAN_TX_PIN, CAN_RX_PIN, TWAI_MODE_NORMAL);
    timing_config = TWAI_TIMING_CONFIG_500KBITS();

    // Manually define message filter
    filter_config.single_filter = true;
    filter_config.acceptance_code = obd2_acceptance_code;
    filter_config.acceptance_mask = obd2_acceptance_mask;

    // Initialization status, reused
    esp_err_t INIT_STATUS;

    // Load configurations
    INIT_STATUS = twai_driver_install( &general_config, &timing_config, &filter_config);
    if( INIT_STATUS != ESP_OK ) {
        Serial.printf("OBD_TASK: Failed to install driver configs\n");
        Serial.printf("OBD_TASK: Error code 0x%04X aka %s\n", (uint16_t)INIT_STATUS, esp_err_to_name(INIT_STATUS));
        while(1) {
            vTaskDelay(pdMS_TO_TICKS(1000));
        }
    }
    Serial.printf("OBD_TASK: Successfully installed driver configs!\n");

    // Start peripheral
    INIT_STATUS = twai_start();
    if( INIT_STATUS != ESP_OK ) {
        Serial.printf("OBD_TASK: Failed to start CAN peripheral\n");
        Serial.printf("OBD_TASK: Error code 0x%04X aka %s\n", (uint16_t)INIT_STATUS, esp_err_to_name(INIT_STATUS));
        while(1) {
            vTaskDelay(pdMS_TO_TICKS(1000));
        }
    }
    Serial.printf("OBD_TASK: Successfully started CAN peripheral!\n");

    // Create buffer
    OBD_COMMAND_QUEUE = xQueueCreate( 10, sizeof(obd_command_t) );
    if(OBD_COMMAND_QUEUE == NULL) {
        Serial.printf("OBD_TASK: Failed to initialize the OBD Library command queue, halting...\n");
        while(1) {
            vTaskDelay(pdMS_TO_TICKS(1000));
        }
    }

    PID_A = 0x05;
    PID_B = 0x0C;
    PID_C = 0x0D;

    esp_err_t poll_result = obd_poll_PIDs();

    // Have the library attempt to determine what PIDs the vehicle supports
    if( poll_result != ESP_OK ) {
        Serial.printf("OBD_TASK: Error polling PIDs, further functionality is not guarunteed\n");
        Serial.printf("OBD_TASK: Error %s\n", esp_err_to_name(poll_result));
    }

    OBD_LIB_READY = true;

}

/**
 * Helper method which loops through the 
 * requested PIDs and uses the OBD-II library
 * to retrieve their values from the vehicle.
 * @param results obd_value_t pointer to store retreived values into
 */
void get_obd_measurements(obd_response_t* response) {
    
    pid_values_t* results = &(response->sensor_values);

    // First measurement
    switch(PID_A) {
            case PID_ENGINE_LOAD_PCT:
                results->PID_A = PID_ENGINE_LOAD_PCT;
                results->uVAL_A = obd_engine_load();
                break;

            case PID_ENGINE_COOLANT_TEMP:
                results->PID_A = PID_ENGINE_COOLANT_TEMP;
                results->uVAL_A = obd_coolant_temp();
                break;
            
            case PID_FUEL_PRESSURE:
                results->PID_A = PID_FUEL_PRESSURE;
                results->uVAL_A = obd_fuel_pressure();
                break;

            case PID_INTAKE_MANUFOLD_PRESSURE:
                results->PID_A = PID_INTAKE_MANUFOLD_PRESSURE;
                results->uVAL_A = obd_intake_pressure();
                break;
            
            case PID_ENGINE_RPM:
                results->PID_A = PID_ENGINE_RPM;
                results->uVAL_A = obd_engine_rpm();
                break;

            case PID_VEHICLE_SPEED:
                results->PID_A = PID_VEHICLE_SPEED;
                results->uVAL_A = obd_vehicle_speed();
                break;
            
            case PID_INTAKE_AIR_TEMP:
                results->PID_A = PID_INTAKE_AIR_TEMP;
                results->uVAL_A = obd_intake_air_temp();
                break;

            case PID_AIR_FLOW_RATE:
                results->PID_A = PID_AIR_FLOW_RATE;
                results->fVAL_A = obd_airflow_rate();
                break;
            
            case PID_THROTTLE_POS:
                results->PID_A = PID_THROTTLE_POS;
                results->uVAL_A = obd_throttle_position();
                break;

            case PID_ENGINE_OIL_TEMP:
                results->PID_A = PID_ENGINE_OIL_TEMP;
                results->uVAL_A = obd_oil_temp();
                break;

            default:
                results->PID_A = 0xFF;
                results->uVAL_A = UINT32_MAX;
                break;
        }

    // Second measurement
    switch(PID_B) {
            case PID_ENGINE_LOAD_PCT:
                results->PID_B = PID_ENGINE_LOAD_PCT;
                results->uVAL_B = obd_engine_load();
                break;

            case PID_ENGINE_COOLANT_TEMP:
                results->PID_B = PID_ENGINE_COOLANT_TEMP;
                results->uVAL_B = obd_coolant_temp();
                break;
            
            case PID_FUEL_PRESSURE:
                results->PID_B = PID_FUEL_PRESSURE;
                results->uVAL_B = obd_fuel_pressure();
                break;

            case PID_INTAKE_MANUFOLD_PRESSURE:
                results->PID_B = PID_INTAKE_MANUFOLD_PRESSURE;
                results->uVAL_B = obd_intake_pressure();
                break;
            
            case PID_ENGINE_RPM:
                results->PID_B = PID_ENGINE_RPM;
                results->uVAL_B = obd_engine_rpm();
                break;

            case PID_VEHICLE_SPEED:
                results->PID_B = PID_VEHICLE_SPEED;
                results->uVAL_B = obd_vehicle_speed();
                break;
            
            case PID_INTAKE_AIR_TEMP:
                results->PID_B = PID_INTAKE_AIR_TEMP;
                results->uVAL_B = obd_intake_air_temp();
                break;

            case PID_AIR_FLOW_RATE:
                results->PID_B = PID_AIR_FLOW_RATE;
                results->fVAL_B = obd_airflow_rate();
                break;
            
            case PID_THROTTLE_POS:
                results->PID_B = PID_THROTTLE_POS;
                results->uVAL_B = obd_throttle_position();
                break;

            case PID_ENGINE_OIL_TEMP:
                results->PID_B = PID_ENGINE_OIL_TEMP;
                results->uVAL_B = obd_oil_temp();
                break;

            default:
                results->PID_B = 0xFF;
                results->uVAL_B = UINT32_MAX;
                break;
        }

    // Third measurement
    switch(PID_C) {
            case PID_ENGINE_LOAD_PCT:
                results->PID_C = PID_ENGINE_LOAD_PCT;
                results->uVAL_C = obd_engine_load();
                break;

            case PID_ENGINE_COOLANT_TEMP:
                results->PID_C = PID_ENGINE_COOLANT_TEMP;
                results->uVAL_C = obd_coolant_temp();
                break;
            
            case PID_FUEL_PRESSURE:
                results->PID_C = PID_FUEL_PRESSURE;
                results->uVAL_C = obd_fuel_pressure();
                break;

            case PID_INTAKE_MANUFOLD_PRESSURE:
                results->PID_C = PID_INTAKE_MANUFOLD_PRESSURE;
                results->uVAL_C = obd_intake_pressure();
                break;
            
            case PID_ENGINE_RPM:
                results->PID_C = PID_ENGINE_RPM;
                results->uVAL_C = obd_engine_rpm();
                break;

            case PID_VEHICLE_SPEED:
                results->PID_C = PID_VEHICLE_SPEED;
                results->uVAL_C = obd_vehicle_speed();
                break;
            
            case PID_INTAKE_AIR_TEMP:
                results->PID_C = PID_INTAKE_AIR_TEMP;
                results->uVAL_C = obd_intake_air_temp();
                break;

            case PID_AIR_FLOW_RATE:
                results->PID_C = PID_AIR_FLOW_RATE;
                results->fVAL_C = obd_airflow_rate();
                break;
            
            case PID_THROTTLE_POS:
                results->PID_C = PID_THROTTLE_POS;
                results->uVAL_C = obd_throttle_position();
                break;

            case PID_ENGINE_OIL_TEMP:
                results->PID_C = PID_ENGINE_OIL_TEMP;
                results->uVAL_C = obd_oil_temp();
                break;

            default:
                results->PID_C = 0xFF;
                results->uVAL_C = UINT32_MAX;
                break;
        }

    // Send results to the proper task
    if( xQueueSend( OBD_RESPONSE_QUEUE, response, pdMS_TO_TICKS(1) ) != pdPASS ) {
        Serial.printf("OBD_TASK: Failed to send values to the receiving task");
    }

    // Serial.printf("OBD_TASK: Sent response!\n");

    return;
}

void get_dtc_helper(obd_command_t* command, obd_response_t* response) {
    
    uint8_t DTC_CNT = 0;

    switch(command->DTC_TYPE) {
        
        case STR_DTC:
            
            if(get_active_dtc(command->DTC_OUT, command->DTC_OUT_MAX, &DTC_CNT) == ESP_OK) {
                response->dtc_valid = true;
                response->dtc_count = DTC_CNT;
            } else {
                response->dtc_valid = false;
                response->dtc_count = 0;
            }

            break;

        case CLR_DTC:

            Serial.printf("OBD_TASK: Control flow should not have reached here\n");
            Serial.printf("OBD_TASK: clearing DTCs should be handled seperate from getting them\n");
            
            break;

        case PEND_DTC:

            if(get_pend_dtc(command->DTC_OUT, command->DTC_OUT_MAX, &DTC_CNT) == ESP_OK) {
                response->dtc_valid = true;
                response->dtc_count = DTC_CNT;
            } else {
                response->dtc_valid = false;
                response->dtc_count = 0;
            }

            break;

        case PRM_DTC:

            if(get_perm_dtc(command->DTC_OUT, command->DTC_OUT_MAX, &DTC_CNT) == ESP_OK) {
                response->dtc_valid = true;
                response->dtc_count = DTC_CNT;
            } else {
                response->dtc_valid = false;
                response->dtc_count = 0;
            }

            break;

        default:
            Serial.printf("OBD_TASK: Invalid DTC request received, ignoring\n");
            response->dtc_valid = false;
            response->dtc_count = 0;
    }

    if( xQueueSend(OBD_RESPONSE_QUEUE, response, pdMS_TO_TICKS(1)) != pdPASS ) {
        Serial.printf("OBD_TASK: Failed to send response\n");
    }

}

void parse_command(obd_command_t* command, obd_response_t* response) {
    
    esp_err_t vin_status = ESP_FAIL;

    switch(command->command_type) {
        
        case START_MON:
            Serial.printf("OBD_TASK: Start monitoring command received!\n");
            isMonitoring = true;
            break;

        case STOP_MON:
            Serial.printf("OBD_TASK: Stop monitoring command received!\n");
            isMonitoring = false;
            break;

        case CHANGE_MON:
            Serial.printf("OBD_TASK: Change PID monitor command received!\n");
            PID_A = command->PID_A;
            PID_B = command->PID_B;
            PID_C = command->PID_C;
            break;

        case GET_DTC:
            Serial.printf("OBD_TASK: Get DTC command received!\n");
            response->response_type = DTC_RESP;
            get_dtc_helper(command, response);
            break;

        case GET_VIN:
            
            Serial.printf("OBD_TASK: Get VIN command received!\n");
            
            // Update response type
            response->response_type = VIN_RESP;
            
            // Clear output and call funct
            memset(command->VIN_OUT, 0, 24);
            vin_status = get_vin( command->VIN_OUT );

            // Check status
            if(vin_status == ESP_OK) {
                response->vin_valid = true;

            } else {
                response->vin_valid = false;
            }

            if( xQueueSend(OBD_RESPONSE_QUEUE, response, pdMS_TO_TICKS(1)) != pdPASS ) {
                Serial.printf("OBD_TASK: Failed to send response\n");
            }

            break;

        case REM_DTC:
            Serial.printf("OBD_TASK: Clear DTC command received!\n");
            clear_dtcs();
            break;

        default:
            Serial.printf("OBD_TASK: Malformed command received\n");
    }
}

void obd_main() {

    obd_task_init();

    // Inter-task communication stuff
    obd_command_t inbound_command;

    isMonitoring = false;

    while (1) {

        obd_response_t response;
        memset(&response, 0, sizeof(obd_response_t));

        // Check for commands
        if(xQueueReceive(OBD_COMMAND_QUEUE, &inbound_command, pdMS_TO_TICKS(1)) == pdPASS) {

            // Parse commands
            parse_command(&inbound_command, &response);

        }

        // Get PID values if enabled
        if(isMonitoring) {
            response.response_type = PID_VALS;
            get_obd_measurements(&response);
        }

        vTaskDelay(pdMS_TO_TICKS(250));
    }
    

}