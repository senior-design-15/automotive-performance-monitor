#include <Arduino.h>
#include "FreeRTOS.h"
#include "freertos/task.h"

#include "obd_task.hpp"
#include "ui_task.hpp"
#include "gps_task.hpp"
#include "lcd_task.hpp"

#include "WiFi.h"

// OBD Task
TaskHandle_t OBD_TASK;

// UI Task
TaskHandle_t UI_TASK;

// GPS Task
TaskHandle_t GPS_TASK;

// LCD Task
TaskHandle_t LCD_TASK;

void setup() {
  
  pinMode(0, INPUT_PULLUP);
  Serial.begin(9600);
  delay(3000);

  if(digitalRead(0) == LOW) {
    Serial.printf("MAIN: Resetting WiFi Configs...\n");
    WiFi.eraseAP();
    WiFi.disconnect();
    Serial.printf("MAIN: Done. Release button to reboot.\n");
    while(digitalRead(0) == LOW) {}
    ESP.restart();
  }

  xTaskCreatePinnedToCore((TaskFunction_t)ui_main, "WEB", 16384, (void*)0, 10, &UI_TASK, 0);
  xTaskCreatePinnedToCore((TaskFunction_t)obd_main, "OBD", 16384, (void*)0, 6, &OBD_TASK, 1);
  xTaskCreatePinnedToCore((TaskFunction_t)gpsMainTask, "GPS", 8192, (void*)0, 10, &GPS_TASK, 1);
  xTaskCreatePinnedToCore((TaskFunction_t)lcd_main, "LCD", 8192, (void*)0, 5, &LCD_TASK, 1);

}

void loop() {
  return;
}
