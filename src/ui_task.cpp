/* Required Libraries */
#include <Arduino.h>
#include <String>				
#include <AsyncTCP.h>           // One part needed for the actual webserver
#include <DNSServer.h>          // Actually handles the captive functionality
#include <ESPAsyncWebServer.h>	// Another part needed for the actual webserver
#include <esp_wifi.h>           // Used as a workaround for a bug or something
#include <SPIFFS.h>				// Used to access HTML and stuff stored in flash
#include <ArduinoJson.h>		// Used in WebSocket reporting

/* Other tasks */
#include "obd_types.hpp"
#include "obd_task.hpp"
#include "gps_task.hpp"
#include "lcd_task.hpp"

/* FreeRTOS and Inter-Process Communication */
#include "FreeRTOS.h"
#include "queue.h"

#define GPS_QUEUE_SIZE 10
#define OBD_QUEUE_SIZE 10

#define MAX_CLIENTS 1
#define WIFI_CHANNEL 9
#define DNS_INTERVAL_MS 30

QueueHandle_t GPS_DATA_QUEUE;
QueueHandle_t OBD_RESPONSE_QUEUE;

// SSID and Password
char ssid[32];
const char* password = NULL;
char redirect_addr[32];

DNSServer dns_server;
AsyncWebServer web_server(80);

// One WebSocket for GPS and OBD
// Use JavaScript to communicate which you need
AsyncWebSocket ws("/ws");

// JSON document used when getting GPS/OBD readings
JsonDocument json_document;

// String where the json document can be serialized
#define JSON_STR_LEN 256
char json_string[JSON_STR_LEN] = {0};

pid_values_t CURRENT_OBD_VALS;
gpsDataPack  CURRENT_GPS_VALS;

const char* PARAM_PID_A = "in_a";
const char* PARAM_PID_B = "in_b";
const char* PARAM_PID_C = "in_c";

void init_dns() {
    dns_server.setErrorReplyCode(DNSReplyCode::NoError);
	dns_server.setTTL(300);
    dns_server.start(53, "*", WiFi.softAPIP());
}

void init_softAP() {

	// Generate SSID from the chip's MAC address
	uint8_t mac_addr[6] = {0};
	esp_read_mac(mac_addr, ESP_MAC_WIFI_SOFTAP);
	sprintf(ssid, "APM%02X%02X", mac_addr[4], mac_addr[5]);
	Serial.printf("UI_TASK: Generated SSID %s\n", ssid);

	// Create the soft AP
	if(!WiFi.softAP(ssid, password)) {
		Serial.printf("UI_TASK: Failed to initialize AP\n");
		while(1){vTaskDelay(pdMS_TO_TICKS(1000));}
	}
	vTaskDelay(pdMS_TO_TICKS(100));

	// Generate the redirect URL
	sprintf(redirect_addr, "http://%s", WiFi.softAPIP().toString());
	Serial.printf("UI_TASK: Generated redirect URL %s\n", redirect_addr);

	// For debugging purposes, print all the important info
	Serial.printf("UI_TASK: AP SSID = %s\n", WiFi.softAPSSID().c_str());
    Serial.printf("UI_TASK: AP IP Address = %s\n", WiFi.softAPIP().toString());
    Serial.printf("UI_TASK: AP MAC Address = %s\n", WiFi.softAPmacAddress().c_str());

    vTaskDelay(pdMS_TO_TICKS(100));

	/*

	This is the old bugfix for android clients. It seems to have broken
	our stuff, but everything seems to work fine with this fix disabled
	using Evan's Google Pixel 8 running Android 14


    esp_wifi_stop();
	vTaskDelay(pdMS_TO_TICKS(100));
	esp_wifi_deinit();
	vTaskDelay(pdMS_TO_TICKS(100));
	wifi_init_config_t my_config = WIFI_INIT_CONFIG_DEFAULT();
	my_config.ampdu_rx_enable = false;
	vTaskDelay(pdMS_TO_TICKS(100));
	esp_wifi_init(&my_config);
	vTaskDelay(pdMS_TO_TICKS(100));
	esp_wifi_start();
	vTaskDelay(pdMS_TO_TICKS(100));
	*/

}

void init_SPIFFS() {
  
  if (!SPIFFS.begin(true)) {
    Serial.println("UI_TASK: Failed to mount SPIFFS, halting\n");
	while(1){vTaskDelay(pdMS_TO_TICKS(100));}
  }

  Serial.println("UI_TASK: SPIFFS mounted!");
}

void ws_send_gps() {

	// clear the existing json document to ensure that it only contain up-to-date information
	json_document.clear();

	char temp_str[32] = {0};

	sprintf(temp_str, "%.5f", CURRENT_GPS_VALS.latDegree);
	json_document["lat"] = temp_str;

	memset(temp_str, 0, 32);
	sprintf(temp_str, "%.5f", CURRENT_GPS_VALS.lonDegree);
	json_document["lon"] = temp_str;

	json_document["speed"] = CURRENT_GPS_VALS.groundSpeed;

	memset(temp_str, 0, 32);
	sprintf(temp_str, "%d:%02d:%02d", CURRENT_GPS_VALS.hours, CURRENT_GPS_VALS.minutes, CURRENT_GPS_VALS.seconds);
	json_document["time"] = temp_str;

	json_document["hdop"] = CURRENT_GPS_VALS.hdop;

	// Use the json_document object to generate the readings and
	// store the output into json_string

	if(serializeJson(json_document, json_string, sizeof(json_string)) > 0)
	{
		// send json_string over the WebSocket to all connected clients.
		ws.textAll(json_string);

		// This is just for debugging
		// Serial.println(json_string);

	} else {
		Serial.println("Failed to serialize JSON");
	}

	// Don't forget to clear the json_document object and
	// json_string either
	memset(json_string, 0, JSON_STR_LEN);
}

void ws_send_obd() {
	// Use the json_document object to generate the readings and
	// store the output into json_string

	// clear the existing json document to ensure that it only contains up-to-date information. 
	json_document.clear();

	// Ensure that the key part of the JSON elements matches
    // the class of the span being replaced. For OBD2 data,
    // the classes are "out_a", "out_b", and "out_c"
	// json_document["out_a"] = "value_a";
	// json_document["out_b"] = "value_b";
	// json_document["out_c"] = "value_c";

	lcd_data_t packet_a;
	lcd_data_t packet_b;
	lcd_data_t packet_c;
	memset(&packet_a, 0, sizeof(lcd_data_t));
	memset(&packet_b, 0, sizeof(lcd_data_t));
	memset(&packet_c, 0, sizeof(lcd_data_t));

	char pid_temp[64];
	memset(pid_temp, 0, 64);

	if(CURRENT_OBD_VALS.PID_A == 0xFF) {
		packet_a.data_type = GPS_DATA;
		packet_a.value = CURRENT_GPS_VALS.groundSpeed;

		json_document["pid_a"] = "GPS Speed";
		json_document["out_a"] = CURRENT_GPS_VALS.groundSpeed;

	} else {
		packet_a.data_type = OBD_DATA;
		packet_a.PID = CURRENT_OBD_VALS.PID_A;
		packet_a.value = CURRENT_OBD_VALS.uVAL_A;
		
		sprintf(pid_temp, "%s", pid_to_str(CURRENT_OBD_VALS.PID_A));
		json_document["pid_a"] = pid_temp;
		if(CURRENT_OBD_VALS.uVAL_A == 255 || CURRENT_OBD_VALS.uVAL_A == 65535) {
			json_document["out_a"] = "Invalid";
		} else {
			json_document["out_a"] = CURRENT_OBD_VALS.uVAL_A;
		}
	}

	if(CURRENT_OBD_VALS.PID_B == 0xFF) {
		packet_b.data_type = GPS_DATA;
		packet_b.value = CURRENT_GPS_VALS.groundSpeed;

		json_document["pid_b"] = "GPS Speed";
		json_document["out_b"] = CURRENT_GPS_VALS.groundSpeed;

	} else {
		packet_b.data_type = OBD_DATA;
		packet_b.PID = CURRENT_OBD_VALS.PID_B;
		packet_b.value = CURRENT_OBD_VALS.uVAL_B;

		memset(pid_temp, 0, 64);
		sprintf(pid_temp, "%s", pid_to_str(CURRENT_OBD_VALS.PID_B));
		json_document["pid_b"] = pid_temp;
		if(CURRENT_OBD_VALS.uVAL_B == 255 || CURRENT_OBD_VALS.uVAL_B == 65535) {
			json_document["out_b"] = "Invalid";
		} else {
			json_document["out_b"] = CURRENT_OBD_VALS.uVAL_B;
		}
	}

	if(CURRENT_OBD_VALS.PID_C == 0xFF) {
		packet_c.data_type = GPS_DATA;
		packet_c.value = CURRENT_GPS_VALS.groundSpeed;

		json_document["pid_c"] = "GPS Speed";
		json_document["out_c"] = CURRENT_GPS_VALS.groundSpeed;

	} else {
		packet_c.data_type = OBD_DATA;
		packet_c.PID = CURRENT_OBD_VALS.PID_C;
		packet_c.value = CURRENT_OBD_VALS.uVAL_C;
		
		memset(pid_temp, 0, 64);
		sprintf(pid_temp, "%s", pid_to_str(CURRENT_OBD_VALS.PID_C));
		json_document["pid_c"] = pid_temp;
		if(CURRENT_OBD_VALS.uVAL_C == 255 || CURRENT_OBD_VALS.uVAL_C == 65535) {
			json_document["out_c"] = "Invalid";
		} else {
			json_document["out_c"] = CURRENT_OBD_VALS.uVAL_C;
		}
	}

	// Serialize the json_document to json_string
	size_t bytes = serializeJson(json_document, json_string, sizeof(json_string));
	if(bytes == 0) {
		Serial.println("UI_TASK: Failed to serialize JSON");
	} else {

		// send the json string over the WebSocket to update the web UI
		ws.textAll(json_string);

		// Send the LCD Data
		xQueueSend(LCD_DATA_QUEUE, &packet_a, pdMS_TO_TICKS(10));
		xQueueSend(LCD_DATA_QUEUE, &packet_b, pdMS_TO_TICKS(10));
		xQueueSend(LCD_DATA_QUEUE, &packet_c, pdMS_TO_TICKS(10));
	}

	// Don't forget to clear the json_document object and
	// json_string either

	// I don't think clearing is necessary as it's overwritten by serializejson each time, 
	// but we can reset json_string: 
	memset(json_string, 0, JSON_STR_LEN);
	
}

void websocket_funct(void *arg, uint8_t *data, size_t len) {
	AwsFrameInfo *info = (AwsFrameInfo*)arg;
  	if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    	data[len] = 0;
    	char* message = (char*)data;

		// Do some logic here to determine if the message is something like OBD or GPS
		// and generate a response accordingly

		// If the websocket request is for obd
		if( strcmp(message, "OBD") == 0 )
		{
			ws_send_obd(); // Fetch and send OBD data
			
		} else if( strcmp(message, "GPS") == 0 )
		{

			// If the websocket request is for gps
			ws_send_gps(); // Fetch and send GPS data
		} else {
			Serial.println("Unknown request received through WebSocket");
		}

		/* The actual sending to WebSocket clients would be handled within the get_obd_readings() and 
		    get_gps_readings() functions after data preparation to avoid old or irrelevant data */
  	}
}

void on_ws_event(AsyncWebSocket* server, AsyncWebSocketClient* client, AwsEventType type, void* arg, uint8_t* data, size_t len) {
	switch (type) {
    	case WS_EVT_CONNECT:
      		Serial.printf("UI_TASK: WebSocket client connected\n");
      		break;
    	case WS_EVT_DISCONNECT:
      		Serial.printf("UI_TASK: WebSocket client disconnected\n");
      		break;
    	case WS_EVT_DATA:
      		websocket_funct(arg, data, len);
      		break;
    	case WS_EVT_PONG:
    	case WS_EVT_ERROR:
      		break;
  	}
}

void init_webserver() {

	ws.onEvent(on_ws_event);

    // Windows 11 workaround
    web_server.on("/connecttest.txt", [](AsyncWebServerRequest *request) { request->redirect("http://logout.net"); });
    web_server.on("/wpad.dat", [](AsyncWebServerRequest *request) { request->send(404); });	

    // Background responses: Probably not all are Required, but some are. Others might speed things up?
	// A Tier (commonly used by modern systems)
	web_server.on("/generate_204", [](AsyncWebServerRequest *request) { request->redirect(redirect_addr); });		    // android captive portal redirect
	web_server.on("/redirect", [](AsyncWebServerRequest *request) { request->redirect(redirect_addr); });			    // microsoft redirect
	web_server.on("/hotspot-detect.html", [](AsyncWebServerRequest *request) { request->redirect(redirect_addr); });    // apple call home
	web_server.on("/canonical.html", [](AsyncWebServerRequest *request) { request->redirect(redirect_addr); });	        // firefox captive portal call home
	web_server.on("/success.txt", [](AsyncWebServerRequest *request) { request->send(200); });					    // firefox captive portal call home
	web_server.on("/ncsi.txt", [](AsyncWebServerRequest *request) { request->redirect(redirect_addr); });			    // windows call home

    // Don't return an icon
    web_server.on("/favicon.ico", [](AsyncWebServerRequest *request) { request->send(404); });

	// Enable the websocket in the web server
	web_server.addHandler(&ws);

    // Initial connection, serve index.html
	web_server.on("/", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/index.html", "text/html");
		Serial.println("UI_TASK: Served Index");
	});

    // Request for the index page
	web_server.on("/index.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/index.html", "text/html");
		Serial.println("UI_TASK: Served index");
	});

	// Request for clear codes page
	web_server.on("/clear-codes.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/clear-codes.html", "text/html");
		Serial.println("UI_TASK: Served clear codes page");
	});

	// Request for the dashboard page
	web_server.on("/dashboard.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/dashboard.html", "text/html");
		Serial.println("UI_TASK: Served dashboard page");
	});

	// Request for the read codes page
	web_server.on("/read-codes.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/read-codes.html", "text/html");
		Serial.println("UI_TASK: Served read codes page");
	});

	// Request for the saved reports page
	web_server.on("/saved-reports.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/saved-reports.html", "text/html");
		Serial.println("UI_TASK: Served saved reports page");
	});

	// Request for the vehicle info page
	web_server.on("/vehicle-info.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/vehicle-info.html", "text/html");
		Serial.println("UI_TASK: Served vehicle info page");
	});

	// Request for the vehicle service page
	web_server.on("/vehicle-service.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/vehicle-service.html", "text/html");
		Serial.println("UI_TASK: Served vehicle service page");
	});

	// Request for the OBD Data page
	web_server.on("/obd-data.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/obd-data.html", "text/html");
		Serial.println("UI_TASK: Served OBD data page");
	});

	// Request for the GPS Data page
	web_server.on("/gps-data.html", HTTP_ANY, [](AsyncWebServerRequest *request) {
		request->send(SPIFFS, "/gps-data.html", "text/html");
		Serial.println("UI_TASK: Served GPS data page");
	});

	web_server.on("/change-mon", HTTP_GET, [](AsyncWebServerRequest *request) {
		if(request->hasParam(PARAM_PID_A) && request->hasParam(PARAM_PID_B) && request->hasParam(PARAM_PID_C)) {
			String val_a = request->getParam(PARAM_PID_A)->value();
			String val_b = request->getParam(PARAM_PID_B)->value();
			String val_c = request->getParam(PARAM_PID_C)->value();
			Serial.printf("UI_TASK: Got param A as 0x%02X\n", val_a.toInt());
			Serial.printf("UI_TASK: Got param B as 0x%02X\n", val_b.toInt());
			Serial.printf("UI_TASK: Got param C as 0x%02X\n", val_c.toInt());

			obd_command_t command;
			memset(&command, 0, sizeof(obd_command_t));
			command.command_type = CHANGE_MON;
			command.PID_A = val_a.toInt();
			command.PID_B = val_b.toInt();
			command.PID_C = val_c.toInt();
			if( xQueueSend(OBD_COMMAND_QUEUE, &command, pdMS_TO_TICKS(100)) != pdPASS ) {
				Serial.printf("UI_TASK: Failed to send command to change sensor monitors\n");
			}


			request->send(SPIFFS, "/obd-data.html", "text/html");
		} else {
			request->send(400, "text/plain", "Missing parameters");
		}
	});

    // the catch all
	web_server.onNotFound([](AsyncWebServerRequest *request) {
		request->redirect(WiFi.softAPIP().toString());
		Serial.print("UI_TASK: onnotfound ");
		Serial.println(request->host());	// This gives some insight into whatever was being requested on the serial monitor
	});

}

void ui_setup() {

	// Initialize GPS Data Queue
	GPS_DATA_QUEUE = xQueueCreate(1, sizeof(gpsDataPack));
	if(GPS_DATA_QUEUE == 0) {
		Serial.println("UI_TASK: Couldn't create GPS Data Queue");
		while(1) {vTaskDelay(pdMS_TO_TICKS(100));}
	}

	// Initialize OBD Data Queue
	OBD_RESPONSE_QUEUE = xQueueCreate(1, sizeof(obd_response_t));
	if(OBD_RESPONSE_QUEUE == 0) {
		Serial.println("UI_TASK: Couldn't create OBD Data Queue");
		while(1) {vTaskDelay(pdMS_TO_TICKS(100));}
	}

	// Wait for OBD task to boot
	Serial.printf("UI_TASK: Waiting for OBD library to initialize\n");
	while(!OBD_LIB_READY) {vTaskDelay(pdMS_TO_TICKS(250));}
	Serial.printf("UI_TASK: OBD Task ready!\n");

	// Wait for GPS task to boot
	Serial.printf("UI_TASK: Waiting for GPS library to initialize\n");
	while(!GPS_COMMAND_READY) {vTaskDelay(pdMS_TO_TICKS(250));}
	Serial.printf("UI_TASK: GPS Task ready!\n");

	// Wait for LCD task to boot
	Serial.printf("UI_TASK: Waiting for LCD library to initialize\n");
	while(!LCD_TASK_READY) {vTaskDelay(pdMS_TO_TICKS(250));}
	Serial.printf("UI_TASK: LCD Task ready!\n");

	// Then initialize the UI
	Serial.printf("UI_TASK: Initializing SPIFFS, this might take a few seconds...\n");
	init_SPIFFS();
	vTaskDelay(pdMS_TO_TICKS(1000));

    init_softAP();
    vTaskDelay(pdMS_TO_TICKS(100));

    init_dns();
    vTaskDelay(pdMS_TO_TICKS(100));

    init_webserver();
    vTaskDelay(pdMS_TO_TICKS(100));

    web_server.begin();


	// Create a command to start monitoring
	obd_command_t command;
	memset(&command, 0, sizeof(obd_command_t));
	command.command_type = CHANGE_MON;
	command.PID_A = PID_ENGINE_RPM;
	command.PID_B = PID_VEHICLE_SPEED;
	command.PID_C = PID_ENGINE_COOLANT_TEMP;

	// Issue command
	if( xQueueSend(OBD_COMMAND_QUEUE, &command, pdMS_TO_TICKS(100)) != pdPASS ) {
		Serial.printf("UI_TASK: Failed to send command to change sensor monitors\n");
	}

	// Reset command and change it to start monitoring
	memset(&command, 0, sizeof(obd_command_t));
	command.command_type = START_MON;

	// Issue command
	if( xQueueSend(OBD_COMMAND_QUEUE, &command, pdMS_TO_TICKS(100)) != pdPASS ) {
		Serial.printf("UI_TASK: Failed to send command to start sensor monitoring\n");
	}
	
}

void ui_main() {

    ui_setup();

	uint32_t task_ticks = 0;

	gpsDataPack  inbound_gps_data;
	obd_response_t inbound_obd_data;

    while(1) {

		// Every loop/tick:
		// Check to see if a new reading is available from OBD task
		//		if there is, update the CURRENT_OBD_VALS variable

		if( xQueueReceive(GPS_DATA_QUEUE, &inbound_gps_data, pdMS_TO_TICKS(1)) == pdPASS ) {
			memcpy(&CURRENT_GPS_VALS, &inbound_gps_data, sizeof(gpsDataPack));
		}


		// Check to see if there is a new GPS reading available
		//		if there is, update a global variable (not created yet)

		if( xQueueReceive(OBD_RESPONSE_QUEUE, &inbound_obd_data, pdMS_TO_TICKS(1)) == pdPASS ) {
			
			// If the request is a PID value, copy the readings locally
			if(inbound_obd_data.response_type == PID_VALS) {
				memcpy(&CURRENT_OBD_VALS, &(inbound_obd_data.sensor_values), sizeof(pid_values_t));
			}

		}

		// Periodically (every 5 ticks in this case) send updates via WebSocket
		// (assuming the websocket has clients)
		if( (task_ticks > 0) && (task_ticks % 5 == 0) ) {
			ws_send_obd();      /* Send latest OBD Data*/
			ws_send_gps();      /* Send latest GPS Data*/

			// WiFi.printDiag(Serial);
			// Serial.printf("UI_TASK: Soft AP SSID: \"%s\"\n", WiFi.softAPSSID());

			// Give clients 10ms to read
			vTaskDelay(pdMS_TO_TICKS(10));
			ws._cleanBuffers();
		}

        dns_server.processNextRequest();
		task_ticks++;
        vTaskDelay(pdMS_TO_TICKS(100));
    }    
    
}