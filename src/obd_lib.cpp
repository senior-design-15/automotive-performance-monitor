#include <Arduino.h>
#include <stdint.h>
#include <string.h>
#include "driver/twai.h"
#include "obd_lib.hpp"

#define OBD2_TX_ID 0x7DF
#define POLL_DELAY_MS 1000

uint8_t PID_01_20_POLLED = 0;  // PIDs 0x01 - 0x20
uint8_t PID_21_40_POLLED = 0;  // PIDs 0x21 - 0x40
uint8_t PID_41_60_POLLED = 0;  // PIDs 0x41 - 0x60
uint8_t PID_61_80_POLLED = 0;  // PIDs 0x61 - 0x80
uint8_t PID_81_A0_POLLED = 0;  // PIDs 0x81 - 0xA0

uint8_t PID_01_20_MASK[4] = {0};    // PIDs 0x01 - 0x20
uint8_t PID_21_40_MASK[4] = {0};    // PIDs 0x21 - 0x40
uint8_t PID_41_60_MASK[4] = {0};    // PIDs 0x41 - 0x60
uint8_t PID_61_80_MASK[4] = {0};    // PIDs 0x61 - 0x80
uint8_t PID_81_A0_MASK[4] = {0};    // PIDs 0x81 - 0xA0

const uint8_t EMPTY_PAYLOAD[4] = {0xAA, 0xAA, 0xAA, 0xAA};

/**
 * Helper method to perform error checking on an OBD-II message
 * @param rx_msg the message to be checked
 * @param request_type the request type of the transmitted message
 * @param PID the parameter ID of the transmitted message
 * @param expected_length expected ISO-TP length of the received message
 * @returns ESP_OK if message is valid, else ESP_ERR_INVALID_RESPONSE
 */
static esp_err_t obd_err_check(twai_message_t* rx_msg, obd2_mode_t request_type, uint8_t PID, uint8_t expected_len) {
    
    // All ISO-TP frames must have a DLC of 8
    if( rx_msg->data_length_code != 8 ) {
        Serial.printf("OBD_LIB: BAD ISO-TP LEN\n");
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Invalid OBD-II message length
    if(rx_msg->data[0] != expected_len) {
        Serial.printf("OBD_LIB: BAD OBD2 LEN\n");
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Negative response indicator
    if( rx_msg->data[1] != 0x40 + (unsigned int)request_type ) {
        Serial.printf("OBD_LIB: NOT A POSITIVE RESPONSE\n");

        return ESP_ERR_INVALID_RESPONSE;
    }

    // Not a response to the expected PID
    if( rx_msg->data[2] != PID ) {
        Serial.printf("OBD_LIB: INCORRECT PID RESPONSE, ");
        Serial.printf("Expected PID 0x%02X ", PID);
        Serial.printf("Got PID 0x%02X\n", rx_msg->data[2] );

        return ESP_ERR_INVALID_RESPONSE;
    }

    return ESP_OK;

}

/**
 * Helper method which determines if a given number lies within an inclusive range
 * @param low bottom inclusive boundary
 * @param high upper inclusive boundary
 * @param x value being tested
 * @returns 0 = false; 1 = true
 */
bool in_range(uint32_t low, uint32_t high, uint32_t x) {
    return (low <= x) && (x <= high);
}

/**
 * Converts an OBD-II message struct to an ESP32 twai message and transmits it. Max TX delay of 100ms
 * Has an optional twai_print statement which is currently enabled
 * @param obd_msg the OBD-II message to be transmitted
 * @returns esp_err_t with status of message transmission
 */
esp_err_t obd2_tx(obd2_t* obd_msg) {
    
    // Create message struct & populate it
    twai_message_t tx_msg;
    tx_msg.extd = 0;
    tx_msg.rtr = 0;
    tx_msg.ss = 0;
    tx_msg.self = 0;
    tx_msg.dlc_non_comp = 0;
    tx_msg.identifier = OBD2_TX_ID;
    tx_msg.data_length_code = 8;

    // Temporary buffer & zero it
    uint8_t TX_DATA[8];
    memset(TX_DATA, 0x00, 8);

    // ISO-TP stuff for OBD-II
    TX_DATA[0] = (obd_msg->len) + 2;
    TX_DATA[1] = obd_msg->mode;
    TX_DATA[2] = obd_msg->PID;

    // Copy in OBD-II message data if any
    for(uint8_t i = 0; i < obd_msg->len; i++) {
        TX_DATA[i+3] = obd_msg->data[i];
    }

    // Copy temporary buffer into outbound message
    memcpy(tx_msg.data, TX_DATA, 8);

    // Length fix for DTC or clear request
    if(obd_msg->mode == STR_DTC) {
        tx_msg.data[0] = 0x01;
    }

    // Length fix for DTC or clear request
    if(obd_msg->mode == PEND_DTC) {
        tx_msg.data[0] = 0x01;
    }

    // Length fix for DTC or clear request
    if(obd_msg->mode == PRM_DTC) {
        tx_msg.data[0] = 0x01;
    }

    // Length fix for DTC or clear request
    if(obd_msg->mode == CLR_DTC) {
        tx_msg.data[0] = 0x01;
    }

    // Attempt to transmit and return any possible errors
    return twai_transmit(&tx_msg, pdMS_TO_TICKS(100));
}

/**
 * Prints an ESP32 twai message to stdout
 * @param msg twai message to be printed
 * @param isRX flag to denote direction
 */
void twai_print(twai_message_t* msg, bool isRX) {

    if(isRX) {
        Serial.printf("[RX] ");
    } else {
        Serial.printf("[TX] ");
    }

    if(msg->dlc_non_comp) {
        Serial.printf("Non-compliant message\n");
        return;
    }

    if(msg->extd) {
        Serial.printf("0x%08X: ", (unsigned int)(msg->identifier));
    } else {
        Serial.printf("0x%03X: ", (unsigned int)(msg->identifier));
    }

    for(int i = 0; i < msg->data_length_code; i++) {
        Serial.printf("0x%02X  ", msg->data[i]);
    }

    Serial.printf("\n");

}

/**
 * Polls OBD-II PID support from 0x01 to 0x20
 * @returns esp_err_t any TX or RX errors
 * 
 * ESP_ERR_INVALID_SIZE = malformed response length
 * ESP_ERR_INVALID_RESPONSE = negative response
 */
static esp_err_t poll_pid_01_20() {

    // Construct OBD-II request
    obd2_t outbound_msg;
    outbound_msg.mode = CUR_DATA;
    outbound_msg.PID = 0x00;
    outbound_msg.len = 0;
    memcpy(outbound_msg.data, EMPTY_PAYLOAD, 4);

    // TX OBD-II request
    esp_err_t STATUS = obd2_tx(&outbound_msg);
    if(STATUS != ESP_OK) {
        return STATUS;
    }

    // Listen for a response up to 1 second
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(POLL_DELAY_MS) );
    if( STATUS != ESP_OK ) {
        return STATUS;
    }

    // Byte 0 = remaining length
    // Byte 1 = 0x40 + mode if success
    // Byte 2 = echo of PID
    // Bytes 3 - 7 = Payload

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, 0x00, 6);
    if( STATUS != ESP_OK ) {
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Store RX'd bitmasks
    PID_01_20_MASK[0] = rx_msg.data[3];
    PID_01_20_MASK[1] = rx_msg.data[4];
    PID_01_20_MASK[2] = rx_msg.data[5];
    PID_01_20_MASK[3] = rx_msg.data[6];

    // Update status flag
    PID_01_20_POLLED = 1;

    // Return success if we made it this far
    return ESP_OK;
}

/**
 * Polls OBD-II PID support from 0x21 to 0x40
 * @returns esp_err_t any TX or RX errors
 * 
 * ESP_ERR_INVALID_SIZE = malformed response length
 * ESP_ERR_INVALID_RESPONSE = negative response
 * ESP_ERR_NOT_SUPPORTED = no PIDs in this range
 */
static esp_err_t poll_pid_21_40() {

    // Construct OBD-II request
    obd2_t outbound_msg;
    outbound_msg.mode = CUR_DATA;
    outbound_msg.PID = 0x20;
    outbound_msg.len = 0;
    memcpy(outbound_msg.data, EMPTY_PAYLOAD, 4);

    // TX OBD-II request
    esp_err_t STATUS = obd2_tx(&outbound_msg);
    if(STATUS != ESP_OK) {
        return STATUS;
    }

    // Listen for a response up to 1 second
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(POLL_DELAY_MS) );
    if( STATUS != ESP_OK ) {
        return STATUS;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, 0x20, 6);
    if( STATUS != ESP_OK ) {
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Store RX'd bitmask
    // Normally the bytes are "reversed" in that bit 0 represents PID 8
    // So swap them such that bit 0 represents PID 1
    PID_21_40_MASK[0] = rx_msg.data[3];
    PID_21_40_MASK[1] = rx_msg.data[4];
    PID_21_40_MASK[2] = rx_msg.data[5];
    PID_21_40_MASK[3] = rx_msg.data[6];

    // Update status flag
    PID_21_40_POLLED = 1;

    // Return success if we made it this far
    return ESP_OK;
}

/**
 * Polls OBD-II PID support from 0x41 to 0x60
 * @returns esp_err_t any TX or RX errors
 * 
 * ESP_ERR_INVALID_SIZE = malformed response length
 * ESP_ERR_INVALID_RESPONSE = negative response
 * ESP_ERR_NOT_SUPPORTED = no PIDs in this range
 */
static esp_err_t poll_pid_41_60() {

    // Construct OBD-II request
    obd2_t outbound_msg;
    outbound_msg.mode = CUR_DATA;
    outbound_msg.PID = 0x40;
    outbound_msg.len = 0;
    memcpy(outbound_msg.data, EMPTY_PAYLOAD, 4);

    // TX OBD-II request
    esp_err_t STATUS = obd2_tx(&outbound_msg);
    if(STATUS != ESP_OK) {
        return STATUS;
    }

    // Listen for a response up to 1 second
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(POLL_DELAY_MS) );
    if( STATUS != ESP_OK ) {
        return STATUS;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, 0x40, 6);
    if( STATUS != ESP_OK ) {
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Store RX'd bitmask
    // Normally the bytes are "reversed" in that bit 0 represents PID 8
    // So swap them such that bit 0 represents PID 1
    PID_41_60_MASK[0] = rx_msg.data[3];
    PID_41_60_MASK[1] = rx_msg.data[4];
    PID_41_60_MASK[2] = rx_msg.data[5];
    PID_41_60_MASK[3] = rx_msg.data[6];

    // Update status flag
    PID_41_60_POLLED = 1;

    // Return success if we made it this far
    return ESP_OK;
}

/**
 * Polls OBD-II PID support from 0x61 to 0x80
 * @returns esp_err_t any TX or RX errors
 * 
 * ESP_ERR_INVALID_SIZE = malformed response length
 * ESP_ERR_INVALID_RESPONSE = negative response
 * ESP_ERR_NOT_SUPPORTED = no PIDs in this range
 */
static esp_err_t poll_pid_61_80() {

    // Construct OBD-II request
    obd2_t outbound_msg;
    outbound_msg.mode = CUR_DATA;
    outbound_msg.PID = 0x60;
    outbound_msg.len = 0;
    memcpy(outbound_msg.data, EMPTY_PAYLOAD, 4);

    // TX OBD-II request
    esp_err_t STATUS = obd2_tx(&outbound_msg);
    if(STATUS != ESP_OK) {
        return STATUS;
    }

    // Listen for a response up to 1 second
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(POLL_DELAY_MS) );
    if( STATUS != ESP_OK ) {
        return STATUS;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, 0x60, 6);
    if( STATUS != ESP_OK ) {
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Store RX'd bitmask
    // Store RX'd bitmask
    // Normally the bytes are "reversed" in that bit 0 represents PID 8
    // So swap them such that bit 0 represents PID 1
    PID_61_80_MASK[0] = rx_msg.data[3];
    PID_61_80_MASK[1] = rx_msg.data[4];
    PID_61_80_MASK[2] = rx_msg.data[5];
    PID_61_80_MASK[3] = rx_msg.data[6];

    // Update status flag
    PID_61_80_POLLED = 1;

    // Return success if we made it this far
    return ESP_OK;
}

/**
 * Polls OBD-II PID support from 0x81 to 0xA0
 * @returns esp_err_t any TX or RX errors
 * 
 * ESP_ERR_INVALID_SIZE = malformed response length
 * ESP_ERR_INVALID_RESPONSE = negative response
 * ESP_ERR_NOT_SUPPORTED = no PIDs in this range
 */
static esp_err_t poll_pid_81_A0() {

    // Construct OBD-II request
    obd2_t outbound_msg;
    outbound_msg.mode = CUR_DATA;
    outbound_msg.PID = 0x80;
    outbound_msg.len = 0;
    memcpy(outbound_msg.data, EMPTY_PAYLOAD, 4);

    // TX OBD-II request
    esp_err_t STATUS = obd2_tx(&outbound_msg);
    if(STATUS != ESP_OK) {
        return STATUS;
    }

    // Listen for a response up to 1 second
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(POLL_DELAY_MS) );
    if( STATUS != ESP_OK ) {
        return STATUS;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, 0x80, 6);
    if( STATUS != ESP_OK ) {
        return ESP_ERR_INVALID_RESPONSE;
    }

    // Store RX'd bitmask
    // Normally the bytes are "reversed" in that bit 0 represents PID 8
    // So swap them such that bit 0 represents PID 1
    PID_81_A0_MASK[0] = rx_msg.data[3];
    PID_81_A0_MASK[1] = rx_msg.data[4];
    PID_81_A0_MASK[2] = rx_msg.data[5];
    PID_81_A0_MASK[3] = rx_msg.data[6];

    // Update status flag
    PID_81_A0_POLLED = 1;

    // Return success if we made it this far
    return ESP_OK;
}

/**
 * Polls OBD-II PID support across the whole range of 0x01 to 0xA0
 * @returns esp_err_t with any TX or RX errors
 */
esp_err_t obd_poll_PIDs() {

    // Poll 0x01 to 0x20
    esp_err_t OBD_STATUS = poll_pid_01_20();
    if(OBD_STATUS != ESP_OK) {
        return OBD_STATUS;
    }

    // Is 0x20 supported?
    OBD_STATUS = obd_pid_supported(0x20);
    switch(OBD_STATUS) {
        
        // 0x20 supported -> advance
        case ESP_OK:
            break;
        
        // 0x20 not supported -> zero remaining and return
        case ESP_ERR_NOT_SUPPORTED:
            memset(PID_21_40_MASK, 0, 4);
            memset(PID_41_60_MASK, 0, 4);
            memset(PID_61_80_MASK, 0, 4);
            memset(PID_81_A0_MASK, 0, 4);

            PID_21_40_POLLED = 1;
            PID_41_60_POLLED = 1;
            PID_61_80_POLLED = 1;
            PID_81_A0_POLLED = 1;

            return ESP_OK;
            break;

        // Other errors -> break and return
        default:
            return OBD_STATUS;
    }

    // Poll 0x21 to 0x40
    OBD_STATUS = poll_pid_21_40();
    if(OBD_STATUS != ESP_OK) {
        return OBD_STATUS;
    }

    // Is 0x40 supported?
    OBD_STATUS = obd_pid_supported(0x40);
    switch(OBD_STATUS) {
        
        // 0x40 supported -> advance
        case ESP_OK:
            break;
        
        // 0x40 not supported -> zero remaining and return
        case ESP_ERR_NOT_SUPPORTED:

            memset(PID_41_60_MASK, 0, 4);
            memset(PID_61_80_MASK, 0, 4);
            memset(PID_81_A0_MASK, 0, 4);

            PID_41_60_POLLED = 1;
            PID_61_80_POLLED = 1;
            PID_81_A0_POLLED = 1;

            return ESP_OK;
            break;

        // Other errors -> break and return
        default:
            return OBD_STATUS;
    }

    // Poll 0x41 to 0x60
    OBD_STATUS = poll_pid_41_60();
    if(OBD_STATUS != ESP_OK) {
        return OBD_STATUS;
    }

    // Is 0x60 supported?
    OBD_STATUS = obd_pid_supported(0x60);
    switch(OBD_STATUS) {
        
        // 0x60 supported -> advance
        case ESP_OK:
            break;
        
        // 0x60 not supported -> zero remaining and return
        case ESP_ERR_NOT_SUPPORTED:

            memset(PID_61_80_MASK, 0, 4);
            memset(PID_81_A0_MASK, 0, 4);

            PID_61_80_POLLED = 1;
            PID_81_A0_POLLED = 1;

            return ESP_OK;
            break;

        // Other errors -> break and return
        default:
            return OBD_STATUS;
    }

    // Poll 0x61 to 0x80
    OBD_STATUS = poll_pid_61_80();
    if(OBD_STATUS != ESP_OK) {
        return OBD_STATUS;
    }

    // Is 0x80 supported?
    OBD_STATUS = obd_pid_supported(0x80);
    switch(OBD_STATUS) {
        
        // 0x80 supported -> advance
        case ESP_OK:
            break;
        
        // 0x80 not supported -> zero remaining and return
        case ESP_ERR_NOT_SUPPORTED:

            memset(PID_81_A0_MASK, 0, 4);

            PID_81_A0_POLLED = 1;

            return ESP_OK;
            break;

        // Other errors -> break and return
        default:
            return OBD_STATUS;
    }

    // Poll 0x81 to 0xA0
    OBD_STATUS = poll_pid_81_A0();
    if(OBD_STATUS != ESP_OK) {
        return OBD_STATUS;
    }

    return ESP_OK;
    
}

/**
 * Determines if the indicated OBD-II PID is supported
 * @param PID OBD-II PID to check support for
 * @returns esp_err_t denoting success or errors
 * 
 * ESP_OK = supported
 * ESP_ERR_NOT_SUPPORTED = not supported
 */
esp_err_t obd_pid_supported(uint8_t PID) {

    uint8_t result = 0;
    uint8_t bit_index = 0;

    // PIDs in the range of 0x01 to 0x20
    if( in_range(0x01, 0x20, PID) ) {
        
        // Byte 0
        if( in_range( 0x01, 0x08, PID ) ) {
            bit_index = 0x08 - PID;
            result = PID_01_20_MASK[0] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 1
        if( in_range( 0x09, 0x10, PID ) ) {
            bit_index = 0x10 - PID;
            result = PID_01_20_MASK[1] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 2
        if( in_range( 0x11, 0x18, PID ) ) {
            bit_index = 0x18 - PID;
            result = PID_01_20_MASK[2] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 3
        if( in_range( 0x19, 0x20, PID ) ) {
            bit_index = 0x20 - PID;
            result = PID_01_20_MASK[3] & (1 << bit_index);
            result >>= bit_index;
        }
    }

    // PIDs in the range of 0x21 to 0x40
    if( in_range(0x21, 0x40, PID) ) {
        
        // Byte 0
        if( in_range( 0x21, 0x28, PID ) ) {
            bit_index = 0x28 - PID;
            result = PID_21_40_MASK[0] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 1
        if( in_range( 0x29, 0x30, PID ) ) {
            bit_index = 0x30 - PID;
            result = PID_21_40_MASK[1] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 2
        if( in_range( 0x31, 0x38, PID ) ) {
            bit_index = 0x38 - PID;
            result = PID_21_40_MASK[2] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 3
        if( in_range( 0x39, 0x40, PID ) ) {
            bit_index = 0x40 - PID;
            result = PID_21_40_MASK[3] & (1 << bit_index);
            result >>= bit_index;
        }

    }

    // PIDs in the range of 0x41 to 0x60
    if( in_range(0x41, 0x60, PID) ) {

        // Byte 0
        if( in_range( 0x41, 0x48, PID ) ) {
            bit_index = 0x48 - PID;
            result = PID_41_60_MASK[0] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 1
        if( in_range( 0x49, 0x50, PID ) ) {
            bit_index = 0x50 - PID;
            result = PID_41_60_MASK[1] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 2
        if( in_range( 0x51, 0x58, PID ) ) {
            bit_index = 0x58 - PID;
            result = PID_41_60_MASK[2] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 3
        if( in_range( 0x59, 0x60, PID ) ) {
            bit_index = 0x60 - PID;
            result = PID_41_60_MASK[3] & (1 << bit_index);
            result >>= bit_index;
        }
    }

    // PIDs in the range of 0x61 to 0x80
    if( in_range(0x61, 0x80, PID) ) {

        // Byte 0
        if( in_range( 0x61, 0x68, PID ) ) {
            bit_index = 0x68 - PID;
            result = PID_61_80_MASK[0] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 1
        if( in_range( 0x69, 0x70, PID ) ) {
            bit_index = 0x70 - PID;
            result = PID_61_80_MASK[1] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 2
        if( in_range( 0x71, 0x78, PID ) ) {
            bit_index = 0x78 - PID;
            result = PID_61_80_MASK[2] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 3
        if( in_range( 0x79, 0x80, PID ) ) {
            bit_index = 0x80 - PID;
            result = PID_61_80_MASK[3] & (1 << bit_index);
            result >>= bit_index;
        }

    }

    // PIDs in the range of 0x81 to 0xA0
    if( in_range(0x81, 0xA0, PID) ) {
        
        // Byte 0
        if( in_range( 0x81, 0x88, PID ) ) {
            bit_index = 0x88 - PID;
            result = PID_81_A0_MASK[0] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 1
        if( in_range( 0x89, 0x90, PID ) ) {
            bit_index = 0x90 - PID;
            result = PID_81_A0_MASK[1] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 2
        if( in_range( 0x91, 0x98, PID ) ) {
            bit_index = 0x98 - PID;
            result = PID_81_A0_MASK[2] & (1 << bit_index);
            result >>= bit_index;
        }

        // Byte 3
        if( in_range( 0x99, 0xA0, PID ) ) {
            bit_index = 0xA0 - PID;
            result = PID_81_A0_MASK[3] & (1 << bit_index);
            result >>= bit_index;
        }

    }

    if(result) {
        return ESP_OK;
    } else {
        return ESP_ERR_NOT_SUPPORTED;
    }

}

/**
 * Retrieves engine load via OBD-II
 * @returns 255 if invalid, otherwise the load as a percent, [0,100]
 */
uint8_t obd_engine_load() {
    
    // Check if PID is supported
    if( obd_pid_supported(PID_ENGINE_LOAD_PCT) == ESP_ERR_NOT_SUPPORTED ) {
        return 0xFF;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_ENGINE_LOAD_PCT;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x04 TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x04 RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_ENGINE_LOAD_PCT, 3);
    if( STATUS != ESP_OK ) {
        return 0xFF;
    }

    // Load (%) = (100*A)/255
    return (100 * rx_msg.data[3]) / 255;
    
}

/**
 * Retrieves engine coolant temperature via OBD-II
 * @returns 255 if invalid, otherwise the temperature in celsius
 *          expected values are 0 to 120
 */
uint8_t obd_coolant_temp() {

    // If PID not supported, return an error value
    if( obd_pid_supported(PID_ENGINE_COOLANT_TEMP) == ESP_ERR_NOT_SUPPORTED ) {
        return 0xFF;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_ENGINE_COOLANT_TEMP;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x05 TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x05 RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_ENGINE_COOLANT_TEMP, 3);
    if( STATUS != ESP_OK ) {
        return 0xFF;
    }

    // Temp (celsius) = A - 40
    return rx_msg.data[3] - 40;

}

/**
 * Retrieves fuel pressure via OBD-II
 * @returns UINT16_MAX if invalid, otherwise the fuel pressure in kPa
 */
uint16_t obd_fuel_pressure() {
    
    // If PID not supported, return an error value
    if( obd_pid_supported(PID_FUEL_PRESSURE) == ESP_ERR_NOT_SUPPORTED ) {
        return UINT16_MAX;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_FUEL_PRESSURE;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x0A TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return UINT16_MAX;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x0A RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return UINT16_MAX;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_FUEL_PRESSURE, 3);
    if( STATUS != ESP_OK ) {
        return UINT16_MAX;
    }

    // Fuel pressure (kPa) = A * 3
    return ((uint16_t)(rx_msg.data[3])) * 3;

}

/**
 * Retrieves intake manufold absolute pressure via OBD-II
 * @returns -1 if invalid, otherwise the pressure in kPa 
 */
int16_t obd_intake_pressure() {
    // If PID not supported, return an error value
    if( obd_pid_supported(PID_INTAKE_MANUFOLD_PRESSURE) == ESP_ERR_NOT_SUPPORTED ) {
        return -1;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_INTAKE_MANUFOLD_PRESSURE;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x0B TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return -1;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x0B RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return -1;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_INTAKE_MANUFOLD_PRESSURE, 3);
    if( STATUS != ESP_OK ) {
        return -1;
    }

    // Manufold Pressure = A
    return rx_msg.data[3];
}

/**
 * Retrieves engine RPM via OBD-II
 * @returns UINT16_MAX if invalid, otherwise the engine RPM
 *          Expected values should be 0 to 10,000
 */
uint16_t obd_engine_rpm() {
    
    // If PID not supported, return an error value
    if( obd_pid_supported(PID_ENGINE_RPM) == ESP_ERR_NOT_SUPPORTED ) {
        return 0xFFFF;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_ENGINE_RPM;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x0C TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFFFF;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x0C RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFFFF;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_ENGINE_RPM, 4);
    if( STATUS != ESP_OK ) {
        return 0xFFFF;
    }

    // RPM = (256A + B)/4
    return ((256 * rx_msg.data[3]) + rx_msg.data[4]) >> 2;
}

/**
 * Retrieves vehicle speed via OBD-II
 * @returns UINT16_MAX if invalid, otherwise the vehicle speed in km/h
 *          Expected values should be 0 to 320
 */
uint16_t obd_vehicle_speed() {
    
    // If PID not supported, return an error value
    if( obd_pid_supported(PID_VEHICLE_SPEED) == ESP_ERR_NOT_SUPPORTED ) {
        return 0xFFFF;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_VEHICLE_SPEED;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x0D TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFFFF;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x0D RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFFFF;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_VEHICLE_SPEED, 3);
    if( STATUS != ESP_OK ) {
        return 0xFFFF;
    }

    // Vehicle speed (km/h) = A
    return (uint16_t)(rx_msg.data[3]);

}

/**
 * Retrieves intake air temperature via OBD-II
 * @returns 255 if invalid, otherwise temperature in celsius 
 *          Expected values should be 0 80
 */
uint8_t obd_intake_air_temp() {
    
    // If PID not supported, return an error value
    if( obd_pid_supported(PID_INTAKE_AIR_TEMP) == ESP_ERR_NOT_SUPPORTED ) {
        return 0xFF;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_INTAKE_AIR_TEMP;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x0F TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x0F RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_INTAKE_AIR_TEMP, 3);
    if( STATUS != ESP_OK ) {
        return 0xFF;
    }

    // Temp (celsius) = A - 40
    return rx_msg.data[3] - 40;

}

/**
 * Retrieves mass airflow rate via OBD-II
 * @returns -1 if invalid, otherwise air flow in grams per second 
 */
float obd_airflow_rate() {
    
    // If PID not supported, return an error value
    if( obd_pid_supported(PID_AIR_FLOW_RATE) == ESP_ERR_NOT_SUPPORTED ) {
        return -1;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_AIR_FLOW_RATE;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x10 TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return -1;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x10 RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return -1;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_AIR_FLOW_RATE, 4);
    if( STATUS != ESP_OK ) {
        return -1;
    }

    // This one is easier for me to code not as a one-liner
    float value = 256 * (uint16_t)(rx_msg.data[3]);
    value += (uint16_t)(rx_msg.data[4]);
    value /= 100;
    return value;

}

/**
 * Retrieves throttle position via OBD-II
 * @returns 0xFF if invalid, otherwise throttle position as a percent
 *          Expected values should be 0 to 100
 */
uint8_t obd_throttle_position() {
    
    // If PID not supported, return an error value
    if( obd_pid_supported(PID_THROTTLE_POS) == ESP_ERR_NOT_SUPPORTED ) {
        return 0xFF;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_THROTTLE_POS;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x11 TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x11 RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_THROTTLE_POS, 3);
    if( STATUS != ESP_OK ) {
        return 0xFF;
    }

    // Throttle position % = (100 * A) / 255
    return ((uint16_t)(rx_msg.data[3]) * 100) / 255;
}

/**
 * Retrieves engine oil temperature via OBD-II
 * @returns 255 if invalid, otherwise temperature in celsius
 *          Expected values should be 0 to 135
 */
uint8_t obd_oil_temp() {

        // If PID not supported, return an error value
    if( obd_pid_supported(PID_ENGINE_OIL_TEMP) == ESP_ERR_NOT_SUPPORTED ) {
        return 0xFF;
    }

    // Construct the OBD-II message
    obd2_t obd_msg;
    obd_msg.mode = CUR_DATA;
    obd_msg.PID = PID_ENGINE_OIL_TEMP;
    obd_msg.len = 0;

    // Transmit OBD-II message & print TX errors if any
    esp_err_t STATUS = obd2_tx(&obd_msg);
    if( STATUS != ESP_OK ) {
        Serial.printf("OBD_LIB: PID 0x5C TX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Wait up to 1s for the response message, print RX errors if any
    twai_message_t rx_msg;
    STATUS = twai_receive( &rx_msg, pdMS_TO_TICKS(1000) );
    if(STATUS != ESP_OK) {
        Serial.printf("OBD_LIB: PID 0x5C RX Failure with error 0x%04X AKA %s\n", (unsigned int)STATUS, esp_err_to_name(STATUS) );
        return 0xFF;
    }

    // Error checking
    STATUS = obd_err_check(&rx_msg, CUR_DATA, PID_ENGINE_OIL_TEMP, 3);
    if( STATUS != ESP_OK ) {
        return 0xFF;
    }

    // Temp (celsius) = A - 40
    return rx_msg.data[3] - 40;
}