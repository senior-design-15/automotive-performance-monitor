/*
*gps_task.h
*main gps header file for data structs and methods
*Brayden Mleziva
*3/25/2024
*/

#include "freertos/queue.h"

struct gpsDataPack
{
    float latDegree;
    float lonDegree;
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
    int groundSpeed;
    uint32_t numSats;
    uint32_t hdop;
};

extern bool GPS_COMMAND_READY;



void gpsMainTask(void *pvParameters);
gpsDataPack gpsPacketBuild();