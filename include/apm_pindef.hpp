#pragma once

#define GPS_TX_PIN 10
#define GPS_RX_PIN 11

#define CAN_TX_PIN (gpio_num_t)4
#define CAN_RX_PIN (gpio_num_t)6

#define LCD_SDA 15
#define LCD_SCL 16

#define LCD_A_ADDR 0x0A
#define LCD_B_ADDR 0x0B
#define LCD_C_ADDR 0x0C