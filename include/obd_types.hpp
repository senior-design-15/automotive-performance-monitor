#ifndef OBD_TYPES_H
#define OBD_TYPES_H

#include <stdint.h>

#define PID_ENGINE_LOAD_PCT 0x04 // Done
#define PID_ENGINE_COOLANT_TEMP 0x05 // Done
#define PID_FUEL_PRESSURE 0x0A // Done
#define PID_INTAKE_MANUFOLD_PRESSURE 0x0B // Done
#define PID_ENGINE_RPM 0x0C // Done
#define PID_VEHICLE_SPEED 0x0D // Done
#define PID_INTAKE_AIR_TEMP 0x0F // Done
#define PID_AIR_FLOW_RATE 0x10 // Done
#define PID_THROTTLE_POS 0x11 // Done
#define PID_ENGINE_OIL_TEMP 0x5C // Done

/**
 * Used internally in conjunction with
 * the obd2_t structure 
 */
typedef enum {
    ERR = 0x00,
    CUR_DATA = 0x01,
    FRZ_DATA = 0x02,
    STR_DTC = 0x03,
    CLR_DTC = 0x04,
    PEND_DTC = 0x07,
    REQ_INF = 0x09,
    PRM_DTC = 0x0A
} obd2_mode_t;

/**
 * Used internally as an abstraction for OBD-II
 * messages that will be transmitted. 
 */
typedef struct {
    obd2_mode_t mode;
    uint8_t PID;
    uint8_t len;
    uint8_t data[4];
} obd2_t;

/**
 * Enum which is used in conjucntion with the command structure.
 * This specifies the functionality that the command will fulfill 
 */
typedef enum {
    START_MON,
    STOP_MON,
    CHANGE_MON,
    GET_DTC,
    REM_DTC,
    GET_VIN
} obd_command_type_t;

/**
 * Structure which should be used to pass commands to
 * the OBD library task.
 * @param command_type enum which determines the function of the current command
 * @param PID_A If the command is CHANGE_MON, this input and the other PID values are
 *              the new values to monitor.
 * @param PID_B If the command is CHANGE_MON, this input and the other PID values are
 *              the new values to monitor.
 * @param PID_C If the command is CHANGE_MON, this input and the other PID values are
 *              the new values to monitor.
 * @param DTC_TYPE If the command is GET_DTC, this input value is used to select between
 *                 active, pending, or stored DTCs.
 * @param DTC_OUT If the command is GET_DTC, this is a buffer where the collected
 *                DTCs should be placed.
 * @param DTC_OUT_MAX If the command is GET_DTC, this input specifies the maximum
 *                    number of DTCs that will fit into the output buffer.
 *                    Internally, 27 DTCs is the maximum supported.
 */
typedef struct {
    obd_command_type_t command_type;
    uint8_t PID_A;
    uint8_t PID_B;
    uint8_t PID_C;
    obd2_mode_t DTC_TYPE;
    uint16_t* DTC_OUT;
    uint8_t DTC_OUT_MAX;
    char* VIN_OUT;
} obd_command_t;

/**
 * Structure containing the values and their associated PIDs.
 * Some PID values are represented as floats, which is why
 * there are unions in here. For most PIDs, you can get the
 * unsigned int value by accessing the corresponding uVAL, or
 * for those PIDs that return floats, access the matching fVAL
 */
typedef struct {
    uint8_t PID_A;
    union {
        float    fVAL_A;
        uint32_t uVAL_A; 
    };
    uint8_t PID_B;
    union {
        float    fVAL_B;
        uint32_t uVAL_B;
    };
    uint8_t PID_C;
    union {
        float    fVAL_C;
        uint32_t uVAL_C;
    };
} pid_values_t;

typedef enum {
    PID_VALS,
    DTC_RESP,
    VIN_RESP
} response_type_t;

typedef struct {
    response_type_t response_type;
    pid_values_t sensor_values;
    bool dtc_valid;
    uint8_t dtc_count;
    bool vin_valid;
} obd_response_t;

#endif