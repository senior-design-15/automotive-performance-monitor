#pragma once
#include <stdint.h>
#include "freertos/queue.h"

extern QueueHandle_t LCD_DATA_QUEUE;
extern bool LCD_TASK_READY;

typedef enum {
    OBD_DATA,
    GPS_DATA
} lcd_type_t;

typedef struct {
    lcd_type_t data_type;
    uint8_t PID;
    uint32_t value;
} lcd_data_t;

void lcd_main();