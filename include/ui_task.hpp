#ifndef UI_TASK_H
#define UI_TASK_H

extern QueueHandle_t OBD_RESPONSE_QUEUE;
extern QueueHandle_t GPS_DATA_QUEUE;

void ui_main();

#endif