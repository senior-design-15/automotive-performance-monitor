#ifndef ISO_TP_H
#define ISO_TP_H

#include <esp_err.h>
#include <stdint.h>

#define DTC_MAX 27

esp_err_t get_active_dtc(uint16_t* dtc_out, uint8_t out_max, uint8_t* actual_cnt);
esp_err_t get_perm_dtc(uint16_t* dtc_out, uint8_t out_max, uint8_t* actual_cnt);
esp_err_t get_pend_dtc(uint16_t* dtc_out, uint8_t out_max, uint8_t* actual_cnt);
esp_err_t get_vin(char str_out[24]);
esp_err_t clear_dtcs();
void dtc_to_string(uint16_t dtc, char string[6]);

#endif