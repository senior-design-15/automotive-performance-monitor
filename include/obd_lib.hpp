#ifndef OBD_LIB_H
#define OBD_LIB_H 1

#include "obd_types.hpp"
#include "driver/twai.h"

extern const uint8_t EMPTY_PAYLOAD[4];

esp_err_t obd2_tx(obd2_t* obd_msg);
void twai_print(twai_message_t* msg, bool isRX);
esp_err_t obd_poll_PIDs();
esp_err_t obd_pid_supported(uint8_t PID);
bool in_range(uint32_t low, uint32_t high, uint32_t x);
uint8_t obd_engine_load();
uint8_t obd_coolant_temp();
uint16_t obd_fuel_pressure();
int16_t obd_intake_pressure();
uint16_t obd_engine_rpm();
uint16_t obd_vehicle_speed();
uint8_t obd_intake_air_temp();
float obd_airflow_rate();
uint8_t obd_throttle_position();
uint8_t obd_oil_temp();

#endif