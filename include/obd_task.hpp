#ifndef OBD_MAIN
#define OBD_MAIN

#include <freertos/queue.h>

extern QueueHandle_t OBD_COMMAND_QUEUE;
extern volatile bool OBD_LIB_READY;

void obd_main();
const char* pid_to_str(uint8_t pid);

#endif